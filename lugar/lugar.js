const axios = require('axios')

const getLugarLatLng = async(direccion) => {
    let encoded = encodeURI(direccion)
    let url = `https://maps.googleapis.com/maps/api/geocode/json?address=${encoded}&key=AIzaSyDPAQxXijzmMTgNj_pwUv3yldgMTI51aQY`


    let resp = await axios.get(url)

    if (resp.data.status == 'ZERO_RESULTS') {
        throw new Error(`No hay resultados para la ciudad ${direccion}`)
    }

    let location = resp.data.results[0]

    return {
        direccion: location.formatted_address,
        lat: location.geometry.location.lat,
        lng: location.geometry.location.lng
    }
}

module.exports = {
    getLugarLatLng
}