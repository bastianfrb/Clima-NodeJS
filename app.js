const argv = require('yargs').options({
    direccion: {
        alias: 'd',
        desc: 'Dirección de la ciudad',
        demand: true
    },

}).argv

const lugar = require('./lugar/lugar')
const clima = require('./clima/clima')

let getInfo = async(direccion) => {
    try {
        let coords = await lugar.getLugarLatLng(direccion)
        let temp = await clima.getClima(coords.lat, coords.lng)

        return `El clima en ${coords.direccion} es ${temp}°C`
    } catch (e) {
        return `No se pudo determinar el clima en ${direccion}`
    }
}

getInfo(argv.direccion)
    .then(msje => console.log(msje))
    .catch(err => console.log(err))

// clima.getClima(-33.4488897, -70.6692655)
//     .then(temp => console.log(temp))
//     .catch(err => console.log(err))

// lugar.getLugarLatLng(argv.direccion)
//     .then(res => {
//         console.log(res)
//     })
//     .catch(err => console.log('', err))