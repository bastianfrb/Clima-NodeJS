const axios = require('axios')

let getClima = async(lat, lng) => {

    let urlClima = `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&units=metric&appid=bea4f80ab82e06ce964c9ae7ed743a5e`

    let resp = await axios.get(urlClima)

    return resp.data.main.temp
}

module.exports = {
    getClima
}